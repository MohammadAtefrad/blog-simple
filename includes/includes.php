<?php
include 'blogpost.php';

function GetBlogPosts($inId=null, $inTagId =null)
{

	// Change this info so that it works with your system.
	$servername = "localhost";
	$username = "root";
	$password = "";
	try {
		$conn = new PDO("mysql:host=$servername;dbname=blog", $username, $password);
		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// echo "Connected successfully";
		}
	catch(PDOException $e)
		{
		// echo "Connection failed: " . $e->getMessage();
		}

	if (!empty($inId))
	{
		$stmt = $conn->prepare("SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC");
    	$stmt->execute();
		
		// $query = mysql_query("SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC"); 
	}
	else if (!empty($inTagId))
	{
		$stmt = $conn->prepare("SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
    	$stmt->execute();

		// $query = mysql_query("SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
	}
	else
	{
		$stmt = $conn->prepare("SELECT * FROM blog_posts ORDER BY id DESC");
    	$stmt->execute();
		
		// $query = mysql_query("SELECT * FROM blog_posts ORDER BY id DESC");
	}
	
	$postArray = array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		$myPost = new BlogPost($row['id'], $row['title'], $row['post'], $row['post'], $row["author_id"], $row["date_posted"]);
		$postArray[]= $myPost;
	}
	return $postArray;
}
?>