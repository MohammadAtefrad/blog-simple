<?php

class Post_model extends Model{

    private $flagConnection=null;
    
    public $id;
    public $title;
    public $post;
    public $author;
    public $tags;
    public $datePosted;

    public function  __construct(){
        $this->flagConnection= parent::__construct();
    }

    // include 'Controllers/includes/blogpost.php';

    function GetBlogPosts($inId=null, $inTagId =null)
    {

        if (!empty($inId))
        {
            $stmt = $this->flagConnection->prepare("SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC");
            $stmt->execute();
            
            // $query = mysql_query("SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC"); 
        }
        else if (!empty($inTagId))
        {
            $stmt = $this->flagConnection->prepare("SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
            $stmt->execute();

            // $query = mysql_query("SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
        }
        else
        {
            $stmt = $this->flagConnection->prepare("SELECT * FROM blog_posts ORDER BY id DESC");
            $stmt->execute();
            
            // $query = mysql_query("SELECT * FROM blog_posts ORDER BY id DESC");
        }
        
        $postArray = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $myPost = $this->BlogPost($row['id'], $row['title'], $row['post'], $row['post'], $row["author_id"], $row["date_posted"]);
            $postArray[]= $myPost;
        }
        return $postArray;
    }


    public function BlogPost ($inId=null, $inTitle=null, $inPost=null, $inPostFull=null, $inAuthorId=null, $inDatePosted=null)
    {

        if(!empty ($inId))
        {
        $this->id = $inId;
        }
        if (!empty($inTitle))
        {
        $this->title = $inTitle;
        }
        if (!empty($inPost))
        {
        $this->post = $inPost;
        }
        if (!empty($inDatePosted))
        {
        $splitDate = explode ("-", $inDatePosted);
        $this->datePosted = $splitDate[1] . "/" . $splitDate[2] . "/" . $splitDate[0];
        }
        if (!empty($inAuthorId))
        {
            $stmt = $this->flagConnection->prepare("SELECT first_name, last_name FROM peoples WHERE id = " . $inAuthorId);
            $stmt->execute();

            // $query = mysql_query("SELECT first_name, last_name FROM people WHERE id = " . $inAuthorId);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->author = $row["first_name"] . " " . $row["last_name"];
        }
        $postTags = "No Tags";
        if (!empty($inId))
        {
            $stmt = $this->flagConnection->prepare("SELECT tags.* FROM tags_has_blog_posts LEFT JOIN (tags) ON (tags_has_blog_posts.tags_id = tags.id) WHERE tags_has_blog_posts.blog_posts_id = " . $inId);
            $stmt->execute();

            // $query = mysql_query("SELECT tags.* FROM blog_post_tags LEFT JOIN (tags) ON (blog_post_tags.tag_id = tags.id) WHERE blog_post_tags.blog_post_id = " . $inId);
        $tagArray = array();
        $tagIDArray = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
        $tagArray[]=$row["name"];
        $tagIDArray[]=$row["id"];
        }
        if (sizeof($tagArray) > 0)
        {
        foreach ($tagArray as $tag)
        {
        $tagArray[]=$row["name"];
        $tagIDArray[]=$row["id"];
        }
        if (sizeof($tagArray) > 0)
        {
        foreach ($tagArray as $tag)
        {
        if ($postTags == "No Tags")
        {
        $postTags = $tag;
        }
        else
        {
        $postTags = $postTags . ", " . $tag;
        }
        }
        }
        }
        $this->tags = $postTags;
        }
    }

}